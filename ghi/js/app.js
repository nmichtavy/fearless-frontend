function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
        <div class="col mb-4">
            <div class="card shadow mb-3">
                <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                    <h5 class="card-title">${name}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                    <p class="card-text">${description}</p>
                </div>
                <div class="card-footer">${starts} - ${ends}</div>
            </div
        </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url);
        if (!response.ok) {
            throw new Error('Response not ok');
        } else {
            const data = await response.json();

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    console.log(details);
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const starts = new Date(details.conference.starts).toLocaleDateString();
                    const ends = new Date(details.conference.ends).toLocaleDateString();
                    const location = details.conference.location.name;
                    const html = createCard(title, description, pictureUrl, starts, ends, location);
                    const row = document.querySelector('.row');
                    row.innerHTML += html;
                }
            }
            // const conference = data.conferences[0];
            // const nameTag = document.querySelector('.card-title');
            // nameTag.innerHTML = conference.name;

            // const detailUrl = `http://localhost:8000${conference.href}`;
            // const detailResponse = await fetch(detailUrl);
            // if (detailResponse.ok) {
            //     const details = await detailResponse.json();

            //     const descTag = document.querySelector('.card-text');
            //     descTag.innerHTML = details.conference.description;

            //     const imageTag = document.querySelector('.card-img-top');
            //     imageTag.src = details.conference.location.picture_url;
        }
    } catch (e) {
        console.error('error', error);
    }
});
